﻿using ConsomiTousi.Domain;
using ConsomiTousi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConsomiTounsi.Web.Controllers
{
    public class PublishComentController : Controller
    {
        IPublishComentService PublishComentService;
        IUserServices UserServices;
        public PublishComentController()
        {
            PublishComentService = new PublishComentService();
            UserServices = new UserServices();
        }

        // GET: PublishComent
        public ActionResult Index()
        {
            return View();
        }

        // GET: PublishComent/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PublishComent/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PublishComent/Create
        [HttpPost]
        public ActionResult Create(PublishComent publishComent)
        {

            return View();
        }

        // GET: PublishComent/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PublishComent/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PublishComent/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PublishComent/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
