﻿using ConsomiTounsi.Web.Models;
using ConsomiTousi.Domain;
using ConsomiTousi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConsomiTounsi.Web.Controllers
{
    public class PublishController : Controller
    {

        IPublishServicecs publishServices;
        IUserServices UserServices;
        IPublishComentService PublishComentService;
        List<PublishVM> lst = new List<PublishVM>();
        public PublishController()
        {
            publishServices = new PublishServicecs();
            UserServices = new UserServices();
            PublishComentService = new PublishComentService();
        }


        // GET: Publish
        public ActionResult Index()
        {
            IEnumerable<Publish> lstpub = publishServices.GetAll();
            foreach (Publish p in lstpub)
            {
                p.MyUsers = UserServices.GetMany(c => c.UsersId == p.UsersId).First();

                PublishVM pub = new PublishVM();
                pub.PublishId = p.PublishId;
                pub.Subjet = p.Subjet;
                pub.UsersId = p.UsersId;
                pub.DatePublish = p.DatePublish;
                pub.PublishComents = PublishComentService.GetMany(c => c.PublishId == p.PublishId).ToList();
                pub.username = p.MyUsers.UsersLastname + " " + p.MyUsers.UsersName;
                pub.nbrcomment = pub.PublishComents.Count;
                lst.Add(pub);

                //    p.PublishComents = PublishComentService.GetMany(c => c.PublishId == p.PublishId).ToList();
            }
                return View(lst);
        }

        // GET: Publish/Details/5
        public ActionResult Details(int id)
        {

            return View(publishServices.GetById(id));
        }

        // GET: Publish/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Publish/Create
        [HttpPost]
        public ActionResult Create(Publish publish)
        {
            if (ModelState.IsValid)
            {
                publish.DatePublish = DateTime.Now;
                publish.UsersId = 1;
                publishServices.Add(publish);
                publishServices.Commit();
            }
            return RedirectToAction("Index");

        }

        // GET: Publish/Edit/5
        public ActionResult Edit(int id)
        {

            return View(publishServices.GetById(id));
        }

        // POST: Publish/Edit/5
        [HttpPost]
        public ActionResult Edit(int id ,Publish publish)
        {

            Publish p = publishServices.GetById(id);
            p.Subjet = publish.Subjet;

            publishServices.Commit();

            return RedirectToAction("Index");

        }

        // GET: Publish/Delete/5
        public ActionResult Delete(int id)
        {

            return View(publishServices.GetById(id));
        }

        // POST: Publish/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            publishServices.Delete(publishServices.GetById(id));
            publishServices.Commit();

            return RedirectToAction("Index");
            
        }
    }
}
