﻿using ConsomiTousi.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsomiTounsi.Web.Models
{
    public class PublishVM
    {
        public int PublishId { get; set; }
        
        public string Subjet { get; set; }

        public DateTime? DatePublish { get; set; }
        public int? UsersId { get; set; }

        public string username { get; set; }



        public int nbrcomment { get; set; }

        virtual public List<PublishComent> PublishComents { get; set; }
    }
}