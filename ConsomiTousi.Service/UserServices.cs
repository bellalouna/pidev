﻿using ConsomiTounsi.Data.Infrastructure;
using ConsomiTousi.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Service
{
    public class UserServices : Service<Users>,  IUserServices 
    {
        static IDataBaseFactory factory = new DataBaseFactory();
        static IUnitOfWork utk = new UnitOfWork(factory);

        public UserServices():base(utk)
        {

        }

    }
}
