﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTounsi.Data.Infrastructure
{
    public interface IRepository<T> where T : class
    {
       
      
     
       
        void Commit();
        T GetById(string id);
      
        IEnumerable<T> GetAll();

        void Add(T entity);
        T GetById(int id);
        T Get(Expression<Func<T, bool>> Condition);
       

        IEnumerable<T> GetMany(Expression<Func<T, bool>> Condition = null,
            Expression<Func<T, bool>> OrderBy = null);
        void Update(T entity);
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> Condition);
    }
}
