﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTounsi.Data.Infrastructure
{
    public class Repository<T> : IRepository<T> where T : class
    {
        ConsomiCtxt ctxt;
        IDbSet<T> dbSet;
        public Repository(IDataBaseFactory dbFactory)
        {
            ctxt = dbFactory.Ctxt;
            dbSet = ctxt.Set<T>();
        }
       
        public void Commit()
        {
            ctxt.SaveChanges();
        }
        public IEnumerable<T> GetAll()
        {
            return dbSet.AsEnumerable();
        }
        public void Add(T entity)
        {
            dbSet.Add(entity);
        }


        public void Delete(Expression<Func<T, bool>> Condition)
        {
            foreach (T entity in dbSet.Where(Condition))
            {
                dbSet.Remove(entity);
            }
        }

        public void Delete(T entity)
        {
            dbSet.Remove(entity);
        }
        public T Get(Expression<Func<T, bool>> Condition)
        {
            return dbSet.Where(Condition).FirstOrDefault();
        }

        public T GetById(string id)
        {
            return dbSet.Find(id);
        }

        public T GetById(int id)
        {
            return dbSet.Find(id);
        }


        public IEnumerable<T> GetMany(Expression<Func<T, bool>> Condition = null, Expression<Func<T, bool>> OrderBy = null)
        {
            // return DbSet.Where(Condition).OrderBy(OrderBy1);
            IQueryable<T> Query = dbSet;
            if (Condition != null)
                Query = Query.Where(Condition);
            if (OrderBy != null)
                Query = Query.OrderBy(OrderBy);
            return Query.AsEnumerable();
        }

        public void Update(T entity)
        {
            dbSet.Attach(entity);
            ctxt.Entry(entity).State = EntityState.Modified;


        }


    }
}
