namespace ConsomiTounsi.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PublishComents", "PublishId", c => c.Int());
            CreateIndex("dbo.PublishComents", "PublishId");
            AddForeignKey("dbo.PublishComents", "PublishId", "dbo.Publishes", "PublishId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PublishComents", "PublishId", "dbo.Publishes");
            DropIndex("dbo.PublishComents", new[] { "PublishId" });
            DropColumn("dbo.PublishComents", "PublishId");
        }
    }
}
