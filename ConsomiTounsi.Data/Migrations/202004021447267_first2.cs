namespace ConsomiTounsi.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PublishComents", "dateCreation", c => c.DateTime());
            AlterColumn("dbo.PublishComents", "Content", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Publishes", "Subjet", c => c.String(nullable: false));
            AlterColumn("dbo.Publishes", "DatePublish", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Publishes", "DatePublish", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Publishes", "Subjet", c => c.String());
            AlterColumn("dbo.PublishComents", "Content", c => c.String());
            DropColumn("dbo.PublishComents", "dateCreation");
        }
    }
}
