namespace ConsomiTounsi.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migrationinitial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Baskets",
                c => new
                    {
                        BasketId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MyProperty = c.Int(nullable: false),
                        CategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.BasketId)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Image = c.String(),
                        Libelle = c.String(),
                        DateProd = c.DateTime(nullable: false),
                        Description = c.String(),
                        Name = c.String(nullable: false, maxLength: 50),
                        Price = c.Double(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Claims",
                c => new
                    {
                        ClaimsId = c.Int(nullable: false, identity: true),
                        ClaimsDate = c.DateTime(nullable: false),
                        Description = c.String(),
                        ClaimsState = c.String(),
                        ProductId = c.Int(),
                        UsersId = c.Int(),
                    })
                .PrimaryKey(t => t.ClaimsId)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .ForeignKey("dbo.Users", t => t.UsersId)
                .Index(t => t.ProductId)
                .Index(t => t.UsersId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UsersId = c.Int(nullable: false, identity: true),
                        UsersName = c.String(),
                        UsersLastname = c.String(),
                        UsersEmail = c.String(),
                        UsersCin = c.String(),
                        UsersAdress = c.String(),
                        UsersPhone = c.String(),
                        UsersPays = c.String(),
                        UsersRegion = c.String(),
                        UsersRole = c.String(),
                        UsersDate = c.DateTime(nullable: false),
                        DateNaissance = c.DateTime(nullable: false),
                        UsersState = c.String(),
                    })
                .PrimaryKey(t => t.UsersId);
            
            CreateTable(
                "dbo.Deliveries",
                c => new
                    {
                        DeliveryId = c.Int(nullable: false, identity: true),
                        DeliveryDate = c.DateTime(nullable: false),
                        DeliveryState = c.String(),
                        OrderId = c.Int(),
                        UsersId = c.Int(),
                    })
                .PrimaryKey(t => t.DeliveryId)
                .ForeignKey("dbo.Orders", t => t.OrderId)
                .ForeignKey("dbo.Users", t => t.UsersId)
                .Index(t => t.OrderId)
                .Index(t => t.UsersId);
            
            CreateTable(
                "dbo.DeliveryMen",
                c => new
                    {
                        DeliveryManId = c.Int(nullable: false, identity: true),
                        UsersName_d = c.String(),
                        UsersLastname_d = c.String(),
                        UsersEmail_d = c.String(),
                        UsersCin_d = c.String(),
                        UsersAdress_d = c.String(),
                        DateRegister_d = c.DateTime(nullable: false),
                        DateNaissance_d = c.DateTime(nullable: false),
                        State_d = c.String(),
                        Phone_d = c.String(),
                        ImageCin_d = c.String(),
                        DeliveryId = c.Int(),
                    })
                .PrimaryKey(t => t.DeliveryManId)
                .ForeignKey("dbo.Deliveries", t => t.DeliveryId)
                .Index(t => t.DeliveryId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.Int(nullable: false, identity: true),
                        OrderDate = c.DateTime(nullable: false),
                        OrderState = c.String(),
                        ProductId = c.Int(),
                        UsersId = c.Int(),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("dbo.Products", t => t.ProductId)
                .ForeignKey("dbo.Users", t => t.UsersId)
                .Index(t => t.ProductId)
                .Index(t => t.UsersId);
            
            CreateTable(
                "dbo.Factures",
                c => new
                    {
                        FacturesId = c.Int(nullable: false, identity: true),
                        UsersId = c.Int(),
                        OrderId = c.Int(),
                        DateAchat = c.DateTime(nullable: false),
                        Prix = c.Double(nullable: false),
                        Tva = c.Double(nullable: false),
                        Ht = c.Double(nullable: false),
                        Total = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.FacturesId)
                .ForeignKey("dbo.Orders", t => t.OrderId)
                .ForeignKey("dbo.Users", t => t.UsersId)
                .Index(t => t.UsersId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.PublishComents",
                c => new
                    {
                        PublishComentId = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        UsersId = c.Int(),
                    })
                .PrimaryKey(t => t.PublishComentId)
                .ForeignKey("dbo.Users", t => t.UsersId)
                .Index(t => t.UsersId);
            
            CreateTable(
                "dbo.Publishes",
                c => new
                    {
                        PublishId = c.Int(nullable: false, identity: true),
                        Subjet = c.String(),
                        DatePublish = c.DateTime(nullable: false),
                        UsersId = c.Int(),
                    })
                .PrimaryKey(t => t.PublishId)
                .ForeignKey("dbo.Users", t => t.UsersId)
                .Index(t => t.UsersId);
            
            CreateTable(
                "dbo.Shops",
                c => new
                    {
                        ShopId = c.Int(nullable: false, identity: true),
                        NameShop = c.String(),
                        DescriptionShop = c.String(),
                        DateShop = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ShopId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Publishes", "UsersId", "dbo.Users");
            DropForeignKey("dbo.PublishComents", "UsersId", "dbo.Users");
            DropForeignKey("dbo.Factures", "UsersId", "dbo.Users");
            DropForeignKey("dbo.Factures", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Deliveries", "UsersId", "dbo.Users");
            DropForeignKey("dbo.Deliveries", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "UsersId", "dbo.Users");
            DropForeignKey("dbo.Orders", "ProductId", "dbo.Products");
            DropForeignKey("dbo.DeliveryMen", "DeliveryId", "dbo.Deliveries");
            DropForeignKey("dbo.Claims", "UsersId", "dbo.Users");
            DropForeignKey("dbo.Claims", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Baskets", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Products", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Publishes", new[] { "UsersId" });
            DropIndex("dbo.PublishComents", new[] { "UsersId" });
            DropIndex("dbo.Factures", new[] { "OrderId" });
            DropIndex("dbo.Factures", new[] { "UsersId" });
            DropIndex("dbo.Orders", new[] { "UsersId" });
            DropIndex("dbo.Orders", new[] { "ProductId" });
            DropIndex("dbo.DeliveryMen", new[] { "DeliveryId" });
            DropIndex("dbo.Deliveries", new[] { "UsersId" });
            DropIndex("dbo.Deliveries", new[] { "OrderId" });
            DropIndex("dbo.Claims", new[] { "UsersId" });
            DropIndex("dbo.Claims", new[] { "ProductId" });
            DropIndex("dbo.Products", new[] { "CategoryId" });
            DropIndex("dbo.Baskets", new[] { "CategoryId" });
            DropTable("dbo.Shops");
            DropTable("dbo.Publishes");
            DropTable("dbo.PublishComents");
            DropTable("dbo.Factures");
            DropTable("dbo.Orders");
            DropTable("dbo.DeliveryMen");
            DropTable("dbo.Deliveries");
            DropTable("dbo.Users");
            DropTable("dbo.Claims");
            DropTable("dbo.Products");
            DropTable("dbo.Categories");
            DropTable("dbo.Baskets");
        }
    }
}
