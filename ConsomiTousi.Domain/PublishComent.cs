﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Domain
{
    public class PublishComent
    {
       

        

        public int PublishComentId { get; set; }

        [StringLength(255)]
        [Required]
        public string Content { get; set; }
        public int? UsersId { get; set; }
        public DateTime? dateCreation { get; set; }



        [ForeignKey("UsersId")]
        public virtual Users MyUsers { get; set; }

        public int? PublishId { get; set; }

        [ForeignKey("PublishId")]
        public virtual Publish Publish { get; set; }
    }
}
