﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Domain
{
    public class Basket
    {



        public int BasketId { get; set; }
        public string Name { get; set; }
        public int MyProperty { get; set; }
        public int? CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category MyCategory { get; set; }

    }
}
