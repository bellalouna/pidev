﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Domain
{
    public class Delivery
    {

        public int DeliveryId { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string DeliveryState { get; set; }
        public int? OrderId { get; set; }

        [ForeignKey("OrderId")]
        public virtual Order MyOrder { get; set; }
        public int? UsersId { get; set; }

        [ForeignKey("UsersId")]
        public virtual Users MyUsers { get; set;}

        public virtual ICollection<DeliveryMan> DeliveryMans { get; set; }
    }
}
