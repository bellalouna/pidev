﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Domain
{
   public class Publish
    {
        public int PublishId { get; set; }
        [MinLength(10)]
        [Required]
        public string Subjet { get; set; }
        public DateTime? DatePublish { get; set; }
        public int? UsersId { get; set; }

        [ForeignKey("UsersId")]
        public virtual Users MyUsers { get; set; }

        virtual public ICollection<PublishComent> PublishComents { get; set; }
    }
}
