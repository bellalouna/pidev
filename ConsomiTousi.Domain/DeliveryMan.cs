﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Domain
{
   public class DeliveryMan
    {

        public int DeliveryManId { get; set; }
        public string UsersName_d { get; set; }
        public string UsersLastname_d { get; set; }
        public string UsersEmail_d { get; set; }
        public string UsersCin_d { get; set; }
        public string UsersAdress_d { get; set; }
        public DateTime DateRegister_d { get; set; }
        public DateTime DateNaissance_d { get; set; }
        public string State_d { get; set; }
        public string Phone_d { get; set; }
        public string ImageCin_d { get; set; }
        public int? DeliveryId { get; set; }

        [ForeignKey("DeliveryId")]
        public virtual Delivery MyDelivery { get; set; }
    }
}
